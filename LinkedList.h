/*
    Linked list and node declarations
    Author: Sohail Khan
*/
#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H


struct Node
{
    void* data_;
    Node* prev_;
    Node* next_;

    Node()
    {
        data_ = NULL;
        prev_ = NULL;
        next_ = NULL;
    }
};


class LinkedList
{
public:
    LinkedList();
        /* default constructor
           Postcondition: listLen_ = 0, first_ = NULL, last_=NULL */
    ~LinkedList();
        /* Destructor: Postcondition: Memory is deallocated*/

    void AddLinkToBack(void* ptr);
        /* Add a pointer to an object to back of list
           A new node object is allocated.
           Postcondition: length incremented
                          list contains additional pointer */
    void* RemoveThisLink(Node *node);
        /*
            Removes a specific link
            Postcondition: length is decremented
                           pointer to data is returned */
    void* RemoveLinkFromFront();
        /* Removes link an returns the pointer to the object
           Postcondition: listLen_ is decremented and list contains 1 fewer
                          object. */
    Node* GetFirstNode();
        /* Get first node in list 
           Postcondition: NULL is returned if list is empty, otherwise node
           is returned            */
    long GetListLength();
        /* returns length of list */
private:
    Node* first_;
    Node* last_;
    long listLen_;
};

#endif
