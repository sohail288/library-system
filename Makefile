CC = g++
CFLAGS = -g -Wall

SOURCE_FILES= $(wildcard *.cpp)


OBJ_FILES = $(patsubst %.cpp, %.o, $(SOURCE_FILES))

all: main

main: $(OBJ_FILES)
	$(CC) $(OBJ_FILES) -o $@

%.o: %.cpp %.h

.PHONY: clean
clean:
	rm -f $(OBJ_FILES)
	rm -f main
