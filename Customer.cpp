/*
    Implementation file for Customer
*/

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
using namespace std;

#include "Customer.h"
#include "Book.h"

Customer::Customer(string fname, string lname, string account_number)
{
    first_name = fname;
    last_name = lname;
    account_num = account_number;

    numBooks = 0;
    balance = 0;
}

float Customer::calculateFee()
{
    float bookFees = 0;

    for (int i = 0; i < numBooks; i++) {
        if (books[i] && books[i]->isOverDue()) {
            bookFees += books[i]->getLateFee();
        }
    }

    return bookFees;
} 

bool Customer::hasLateBooks()
{
    bool hasLateBooks = false;

    for (int i = 0; i < numBooks; i++) {
        if (books[i] && books[i]->isOverDue()) {
            hasLateBooks = true;
            break;
        }
    }

    return hasLateBooks;
}

string Customer::getFirstName()
{
    return first_name;
}

string Customer::getLastName()
{
    return last_name;
}

string Customer::getAccountNum()
{
    return account_num;
}

int Customer::getNumBooks()
{
    return numBooks;
}

void Customer::updateBalance(float adjustment)
{
    balance += adjustment;
}

float Customer::getBalance()
{
    return balance;
}

bool Customer::canCheckout()
{
    bool hasLateBooks_ = hasLateBooks();
    return balance <= 0 && numBooks < BOOK_LIMIT && !hasLateBooks_;
}

void Customer::addBookToAccount(Book* book)
{
    if (numBooks < BOOK_LIMIT) {
        books[numBooks++] = book;
    } else {
        cout << "CAN'T HAVE MORE BOOKS!" << endl;
    }
}

void Customer::destroyBooks()
{
    for (int i = 0; i < numBooks; i++) {
        if (books[i]) {
            delete books[i];
        }
    }
}

// Helper function to display books
int getBookIndex(Book** books, int numBooks)
{
    int indexOfBook;

    cout << "Which book do you want to choose?" << endl;
    cout << "Enter 0 (zero) to return to previous menu" << endl;
    printf("%20s %20s %15s %10s\n", "Title", "Author", "Catalog#", "DUE DATE");
    for (int i = 0; i < numBooks; i++) {
        cout << i+1 << ") ";
        books[i]->printBook();
    }
    cout << "->";
    cin >> indexOfBook;

    return indexOfBook - 1;
    
}

Book* Customer::removeBookFromAccount()
{
    Book* bookToRemove;
    int indexOfBook;

    indexOfBook = getBookIndex(books, numBooks);

    if (indexOfBook < -1 || indexOfBook >= numBooks) {
        cout << "BOOK DOESN'T EXIST" << endl;
        return NULL;
    } else if (indexOfBook == -1) {
        return NULL;
    }
    // Get the book we want and decrement numBooks;
    bookToRemove = books[indexOfBook];
    books[indexOfBook] = NULL;
    numBooks--;

    // shift over remaining books
    for (int i = indexOfBook; i < BOOK_LIMIT - 1; i++) {
        books[i] = books[i+1]; 
    }

    // set last element to zero, since if you take out a book, and you shift
    // all remaining ones to the left, you will have one empty pocket at the end
    books[BOOK_LIMIT-1] = NULL;

    return bookToRemove;
}

void Customer::appendToFile(ofstream& outFile)
{
    if (!outFile) {
        cout << "Can't write to outfile!" << endl; 
        std::exit(1);
    }
    
 
    outFile << setprecision(2);
    outFile << "<" << first_name << ", " << last_name << ", " 
            << account_num << ", " << balance << ">"
            << endl;

    // write the books
    for (int i = 0; i < numBooks; i++) {
        books[i]->appendToFile(outFile);
    }
    outFile << endl;

}

