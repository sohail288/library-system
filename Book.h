/*
    Book interface

    In addition to normal book-keeping stuff, the book class has to
    hold the checkout date and methods related to checking to see if a
    book is overdue or not
*/
#ifndef _BOOK_H
#define _BOOK_H

#include <ctime>
using namespace std;

// This will be defined in the Library source file
extern time_t currentLibTime;

class Book
{

public:
    string getTitle();
    string getAuthor();
    string getCatalogNum();

    bool isOverDue(time_t currentTime = currentLibTime) const;
        /* calculates the difference in time and if the difference in time is
           negative, returns True */
    float getLateFee();
        /* gets late fee for books */
    void setCheckoutDate(time_t checkoutDate);
        /* sets checkout date to some time, usually now */
    void setDueDate(int numWeeks);
        /* Sets due date 2 weeks in advance */
    char* getDueDate() const;
        /* convert due date to readable format */
    char* getCheckoutDate() const;
        /* convert checkout date to readable format */
    void printBook();
        /* Get a string reprentation of a book */
    void appendToFile(ofstream& outFile);
        /* appends book info to outFile */

    // constructor
    Book(string author = "", string title = "", string catalogNo = "");
private:
    time_t checkout_date_;
    time_t due_date_;
    string author_;
    string title_;
    string catalogNum_;
};
     
#endif
