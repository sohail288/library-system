/*
    Interface file for library

    The library class will deal with the interactivity between various parts
    of a library system.  It will contain a linkedlist of customer accounts, 
    and to validate a user, it will search though linked list with a given
    user acct no and try to match.  It then sets the current user to this
    user.

    It also has a linked list of BookRecord objects that keep track of a 
    particular book info. When the checkout behavior is invoked, it 
    searches the LinkedList for the book name, and then the BookRecord object
    associated with the particular name (catalog number) creates a book
    and associates it with the current customer.

    A user logs out and the current customer is reset
*/

#ifndef _LIBRARY_H
#define _LIBRARY_H

#include <string>
using namespace std;

#include "Customer.h"
#include "BookRecord.h"
#include "Book.h"
#include "LinkedList.h"

class Library
{
public:
    static Customer* currentCustomer;

    // customer related
    void      checkoutBook();
    void      returnBook();
    void      payFee();
    Customer* searchCustomer(string accountNum);

    // book related
    void        createNewBook();
    BookRecord* searchBook(string catalogNum = "");

    // authentication
    bool validateUser(string accountNum);
        /* if user is valid, this sets the currentCustomer 
           it then returns true so that userInterface can be used 
           returns false otherwise */
    bool validateAdmin(string password);
        /* returns true if password matches admin password */
    void createAccount();
        /* interface for making a new user, new user is inserted into
           customer list, but the user must remember their account num!*/

    // displays
    void printBooks();
    
    // interface
    void userInterface();
    void adminInterface();

    // loader functions
    void load_customers();
        /* Loads from customerDBFile Path

           This file is formated in the following way:
            <fName, lName, accountNum, balance>
            book1CatNum, book1CheckoutDate
            book2CatNum, book2CheckoutDate and etc...
        */
    
    void load_books();
        /* Loads from bookDBFilePath
            
            This file is formatted in the following way:
            bookTitle, bookAuthor, bookCatNum, booksIn, booksOut
        */

    // loading method
    void init();
        /*
            Helper method to load books and users. 
            calls load_customers and load_books
        */

    // dumping methods
    void dump_customers();
        /* outputs current customers in system to the customerDBFilePath file
           all contents of customerDBFilePath are erased
        */
    void dump_books();
        /* outputs current books in system to the bookDBFilePath file
           all contents of bookDBFilePath are erased 
        */

    // constructor
    Library(string customerDBFilePath = "custDB.txt", 
            string bookDBFilePath     = "bookDB.txt");

    // Desctructor
    ~Library();
    /* deallocate the customer linked list and the account linked List */
    void clearBookList();
        /* clears bookrecords allocated */
    void clearCustomerList();
        /* clears customer objects and their associated books */

private:
    string adminPass;
    string customerDatabaseFile;
    string bookDatabaseFile;

    LinkedList customerList;
    LinkedList bookRecords;
};
    
#endif 
