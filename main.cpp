/*

    Library program

    To begin,
    You should have an empty or properly formatted custDB.txt file 
    and also an empty or properly formatted bookDB.txt

    In use,
    The main menu allows a user to create a user account. The user
    has to remember their accountNum as that will be used to login to
    the user interfaces. ANY ACCOUNT# FORMAT CAN BE USED

    Also in the main menu you can login to the Library Managment interface.
    The default admin password is: password.
        This can be changed in the Library.cpp file.
    The admin console currently allows you to add a new book to the library.
        A book needs an author, title, catalogNum, and number of books in stock

    The main menu also allows you to exit the system.  It's at this point,
    any changes made to the Library are saved to the database files.

    In the user interface,
    Using the sample account that has account# 1234, login to the system
    The sample account has 0 books checked out and does not have any late fees.

    The user interface allows you to check which books are available to 
    checkout.  Try checking out a book that is included in the sample book
    file (catalog# 123-32 works).  
    Once a book has been checked out, the user interface reflects
    that the current user has one book out. User can't check out a book
    that doesn't have any copies available.

    The user can have a fixed limit of books (default is 5).  They can't
    go over this limit.  To return books, you can select the appropriate 
    command and then you will see a list of books you've checked out with
    their corresponding due dates. To exit this, just enter 0.  

    Finally, a user can logout.  

    LATE FEES
    Late fees are assessed only after a book has been returned.  This way
    redundant fees are not piled on.  To test this feature, go into the
    bookDBFile and change the due date so that it is at least 2 weeks behind
    today.  Once a user returns the late book, they will have to pay any fees
    to be able to use the system again.

    Author: Sohail Khan
    Source: KN KING Modern C (help with time_t and struct tm)
    
*/


/* Includes go here */
#include <iostream>
#include <string>
using namespace std;

#include "Library.h"

#define MAX_LEN 255

// Protos

char mainMenu();
void getCredentials(char *credentials);

int main()
{
    bool userQuit = false;
    char userCredentials[MAX_LEN];
     

    // initialize the library
    Library library;
    library.init();

    while (!userQuit) {
        
        switch (mainMenu()) {
            
            case 'a':
                getCredentials(userCredentials);
                if (library.validateUser(userCredentials)) {
                    library.userInterface();
                }
                else {
                    cout << "You are not in the system" << endl;
                    cout << "create your account" << endl;
                }
                break;
            case 'b':
                cout << "Create an account" << endl;
                library.createAccount();
                break;
            case 'c':
                getCredentials(userCredentials);
                if (library.validateAdmin(userCredentials)) {
                    /* Library Admin */;
                    cout << "You are at admin console" << endl;
                    library.adminInterface();
                } else {
                    cout << "Wrong admin credentials" << endl;
                }
                break;
            case 'd':
                userQuit = true;
                cout << "Thank you for using the library system" << endl;
                library.dump_customers();
                library.dump_books();
                break;
            default:
                cout << "That was not a valid selection" << endl;
                return 1;
                break;
        }

    }

    return 0;

}

char mainMenu()
{
    char rv;
    cout << "Main Menu" << endl;
    cout << "a) Login to your account" << endl;
    cout << "b) Create an account" << endl;
    cout << "c) Library Management" << endl;
    cout << "d) Exit" << endl;
    cout << "Your entry: ";
    cin >> rv;
    return rv;
}



void getCredentials(char *userCredentials)
{
    cout << "Enter your account number or library password" << endl;
    cout << "--> " ;
    
    cin >> userCredentials;
}
