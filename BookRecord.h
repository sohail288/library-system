/*
    Book record interface

    Creation of books
*/

#ifndef _BOOK_RECORD_H
#define _BOOK_RECORD_H 

#include <string>
using namespace std;

#include "Book.h"

class BookRecord
{

public:
    BookRecord operator++();
    BookRecord operator--();
    // accessors
    int getCopiesOut();
    int getCopiesIn();
    int getTotalCopies();

    string getCatalogNum();

    // constructor
    BookRecord(string catalogNo, string Author, string Title, 
               int in = 0, int out = 0); 

    // create books
    Book* createBookCopy();

    void printRecord();
        /* prints record */
    void appendToFile(ofstream& outFile);
        /* adds record to file */

private:
    string catalogNum_;
    string author_;
    string title_;
    int in_;
    int out_;
};

#endif
