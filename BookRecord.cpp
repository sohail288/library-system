/*
    Implementation File for BookRecord.h
*/


#include <string>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
using namespace std;

#include "BookRecord.h"
#include "Book.h"

BookRecord::BookRecord(string catalogNum, string author, string title, 
                      int in, int out)
{
    catalogNum_ = catalogNum; 
    author_ = author;
    title_ = title;
    in_ = in;
    out_ = out;
}

int BookRecord::getCopiesOut()
{
    return out_;
}

int BookRecord::getCopiesIn()
{
    return in_;
}

string BookRecord::getCatalogNum()
{
    return catalogNum_;
}

int BookRecord::getTotalCopies()
{
    return in_ + out_;
}

Book* BookRecord::createBookCopy()
{
    Book* newBook = new Book(author_, title_, catalogNum_);

    return newBook;
}

BookRecord BookRecord::operator++()
{
    // increment books in, and subtract books out
    ++in_;
    --out_;

    return *this;
}

BookRecord BookRecord::operator--()
{
    // increment out and decrement in
    ++out_;
    --in_;

    return *this;
}

void BookRecord::printRecord()
{
    static const char* format = "%-10s %-20s %-35s %5d %5d\n";    

    printf(format, catalogNum_.c_str(), author_.c_str(), title_.c_str(), 
          in_, out_);

}

void BookRecord::appendToFile(ofstream& outFile)
{
    if (!outFile) {
        cout << "Couldn't write book record to file" << endl;
        std::exit(1);
    }

    outFile << title_ << ", " << author_ << ", " << catalogNum_ 
            << ", "   << in_  << ", " << out_ 
            << endl;
}

