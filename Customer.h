/*
    Interface for customer
    A customer object has an account number, name, and a list of 
    books associated with it.
*/

#ifndef _CUSTOMER_H
#define _CUSTOMER_H

#include <string>
using namespace std;

#include "Book.h"

#define BOOK_LIMIT 5


class Customer
{

public:
    float  calculateFee();
        /* iterates overbooks to see which books are over due and by how much */
    string getFirstName();
        /* returns first name */
    string getLastName();
        /* returns last name */
    string getAccountNum();
        /* returns account num */
    int getNumBooks();
        /* returns number of books cust has*/
    void   updateBalance(float);
        /* updates balance after fee payment or new books over due */
    float getBalance();
        /* returns balance */
    bool canCheckout();
        /* Checks to see if customer can checkout a book */
    bool hasLateBooks();
        /* returns true if customer has books that are overdue */
    void appendToFile(ofstream& outFile);
        /* append to ofstream object */
    
    void addBookToAccount(Book* book);
        /* Adds a book to customer account if customer has enough space 
           in array
           numBooks is incremented */
    Book* removeBookFromAccount();
        /*
            prompts user to seleect which book they want to remove
            a book is removed from the array if the correct index is chosen
            numBooks is decremented */
    void destroyBooks();
        /*
            books are cleared from memory
            ONLY USE FOR PROGRAM TERMINATION 
        */

    
    Customer(string fname, string lname, string account_number);
        /*
            A basic constructor 
        */


private:
    string first_name;
    string last_name;
    string account_num;
    float  balance;
    int    numBooks;
    Book*  books[BOOK_LIMIT];
};

#endif
