/*
    Implementation file for LinkedList.h
    
    contains definitions for LinkedList functions
    Author: Sohail Khan

*/

#include <iostream>
#include <cstdlib>
using namespace std;

#include "LinkedList.h"

// Default constructor
LinkedList::LinkedList()
{
    listLen_ = 0;
    first_   = NULL;
    last_    = NULL;
}

// destructor
LinkedList::~LinkedList()
{
    Node *current, *previous;
    if (first_) {
        // list is non-empty so delete nodes
        // start by setting current to first and previous to null
        current  = first_;
        previous = NULL; 
        while (current != NULL) {
            // set previous to point to current and then increment current
            // delete previous data and link
            previous = current;
            current = current->next_;
            delete previous;
        }
    }
}

void LinkedList::AddLinkToBack(void* ptr)
{
    Node* newNode = new Node();
    if (!newNode) {
        cout << "Memory allocation error!" << endl;
        std::exit(1);
    }
    newNode->data_ = ptr;
    
    if (first_ == NULL) {
        //empty list, set first_ and last_ to be the same
        newNode -> prev_ = NULL;
        first_ = newNode;
        last_  = newNode;
    } else {
        // otherwise the last node is pointing to the nth node
        // make the nth node point to newNode
        newNode -> prev_ = last_;
        last_->next_ = newNode; 
        // make the last node the newNode
        last_ = newNode;
    }
    // increment the list length
    listLen_++;

}


void* LinkedList::RemoveLinkFromFront()
{
    // set another pointer to the data held by first node
    void *returnData = first_->data_;
    // set a temp pointer to first_ node
    Node* previousFirst = first_;
    // make first_ the next node in the list
    first_ = first_->next_; 
    // and delete the previous first node 
    delete previousFirst;
    --listLen_; 

    return returnData;
}

void* LinkedList::RemoveThisLink(Node *node)
{
    // go through the linked list
    Node *current = GetFirstNode();
    void *returnData = node->data_;

    while (current && current != node) {
        // find the node 
        current = current->next_;
    }

    if (!current) {
        cout << "Node does not exist" << endl;
        std::exit(1);
    }
    // check three cases, beginning node, middle node, or end node
    if (current == first_) {
        first_ = current->next_;
        if (first_)
            first_->prev_ = NULL;
    } else if (current == last_) {
        last_ = current->prev_;
        last_->next_ = NULL;
    } else {
        current->prev_->next_ = current->next_;
        current->next_->prev_ = current->prev_;
    }
    --listLen_;
    delete current;
    
    // splice and join the list.  Return pointer to data, node->data
    return returnData;

}
    


Node* LinkedList::GetFirstNode()
{
    // if list is empty return NULL value
    if (listLen_) {
        return first_;
    } else {
        return NULL;
    }

}

long LinkedList::GetListLength()
{
    return listLen_;
}
