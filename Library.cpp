/*
    Implementation file for Library

    Split up into manageable sections

    defines currentTime used in Books.h
*/

#include <cstdio>
#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include "Library.h"

#define ENTRY_SZ 255
#define WEEK_LIMIT 2

static const char *ADMIN_PASSWORD = "password";

time_t currentLibTime = time(NULL);

Customer* Library::currentCustomer = NULL;

Library::Library(string customerDBFilePath, string bookDBFilePath)
{
    customerDatabaseFile = customerDBFilePath;
    bookDatabaseFile     = bookDBFilePath;
    adminPass            = string(ADMIN_PASSWORD);
}




Library::~Library()
{
    clearBookList();
    clearCustomerList();
}

void Library::clearBookList()
{
    BookRecord *currentBookRecord;

    if (bookRecords.GetFirstNode() == NULL) {
        return;
    } else {
        currentBookRecord = static_cast<BookRecord*>(
                                        bookRecords.RemoveLinkFromFront());
        delete currentBookRecord;
        clearBookList();
    }
}

void Library::clearCustomerList()
{
    Customer *currentCustomer;
    
    if (customerList.GetFirstNode() == NULL) {
        return;
    } else {
        currentCustomer = static_cast<Customer*>(
                                            customerList.RemoveLinkFromFront());
        currentCustomer->destroyBooks();
        delete currentCustomer;
        clearCustomerList();
    }
}


/***************************************************
    Customer Related methods
***************************************************/
Customer* Library::searchCustomer(string accountNum)
{
    Node* currentLink;
    Customer* curCustomer;

    currentLink = customerList.GetFirstNode();

    while (currentLink != NULL) {
        curCustomer = static_cast<Customer*>(currentLink->data_); 
        
        if (curCustomer->getAccountNum() == accountNum) {
            break;
        }
        currentLink = currentLink->next_;
    }

    if (!currentLink) {
        curCustomer = NULL;
    }

    return curCustomer;
}

void Library::payFee()
{
    float amountToPay;
    if (!currentCustomer) {
        cout << "Can't pay if you aren't logged in!" << endl;
        std::exit(1);
    }

    cout << "Your balance is: $" << currentCustomer->getBalance() << endl;
    cin.ignore();
    cout << "How much would you like to pay? ";    
    cin >> amountToPay;

    // pay a negative amount
    currentCustomer->updateBalance(0-amountToPay);
}

void Library::checkoutBook()
{
    BookRecord* bookrecordToCheckout; 
    Book* checkedOutBook;

    // check to see if customer has privileges
    if (currentCustomer->canCheckout()) {
        // Search for a book, without args = book picked through prompt    
        bookrecordToCheckout = searchBook();
        if (bookrecordToCheckout && bookrecordToCheckout->getCopiesIn()) {
            checkedOutBook = bookrecordToCheckout->createBookCopy();
            // set due dates
            checkedOutBook->setCheckoutDate(time(NULL)); // checked out now
            checkedOutBook->setDueDate(WEEK_LIMIT); // Due in n weeks
            currentCustomer->addBookToAccount(checkedOutBook);

            cout << "You checked out " << checkedOutBook->getTitle() << endl;
            // decrement bookrecord
            --(*bookrecordToCheckout);
        } else {
            cout << "Book not found or there are no copies left" << endl; 
        }
    } else {
        cout << "You cannot check out any books" << endl;
        cout << "Pay fees and/or return books" << endl;
    }
}

void Library::returnBook()
{
    Book* bookToReturn;
    BookRecord* bookrecord;
    string catalogNum;

    // see if the customer has books to return
    if (currentCustomer->getNumBooks() > 0) {
        // remove from the customers account
        bookToReturn = currentCustomer->removeBookFromAccount(); 
    }
    else {
        cout << "You don't have any books to return!" << endl;
        return;
    }

    if (!bookToReturn) {
        return;
    }
    // Now you have to search the library catalog to find the book
    catalogNum = bookToReturn->getCatalogNum();
    
    bookrecord = searchBook(catalogNum);

    if (!bookrecord) {
        cout << "This book is not in our records." << endl;
        cout << "But we'll get rid of it for you." << endl;
    } else {
        // update book record details
        // More books in stock, increment in and decrement out
        ++(*bookrecord);
    }

    // book has been returned, so update the balance if it was late
    if (bookToReturn->isOverDue()) {
        currentCustomer->updateBalance(bookToReturn->getLateFee());
    }
    cout << "You returned book with catalog #" << bookToReturn->getCatalogNum()
         << endl;

    delete bookToReturn;
}


/*****************************************
    Book Related methods
*****************************************/
void Library::createNewBook()
{
    // add a book record 

    string bookTitle, bookAuthor, bookCatalogNum;
    int copiesOnHand;
    BookRecord* newBook;

    cout << "Enter the title";
    cout << "-->";
    cin.ignore();
    getline(cin, bookTitle);

    cout << bookTitle << endl;

    cout << "Enter the author";
    cout << "-->";
    getline(cin, bookAuthor);

    cout << bookAuthor << endl;

    cout << "Enter the catalog number";
    cout << "-->";
    getline(cin, bookCatalogNum);

    cout << bookCatalogNum << endl;

    cout << "Enter number of copies on hand: ";
    cin >> copiesOnHand;

    newBook = new BookRecord(bookCatalogNum, bookAuthor, bookTitle, 
                             copiesOnHand);
    if (!newBook) {
        cout << "Couldn't create a new book" << endl;
        std::exit(1);
    }

    bookRecords.AddLinkToBack(newBook);

}

BookRecord* Library::searchBook(string catalogNum)
{
    Node *currentLink;
    BookRecord *currentBookRecord;
    string inputCatalogNum;

    if (catalogNum == "") {
        cout << "Enter the book catalog number: ";
        cin >> inputCatalogNum;
    } else {
        inputCatalogNum = catalogNum;
    }

    currentLink = bookRecords.GetFirstNode();

    while (currentLink != NULL) {
        currentBookRecord = static_cast<BookRecord*>(currentLink->data_);

        if (inputCatalogNum == currentBookRecord->getCatalogNum()) {
            break;
        }
    
        currentLink = currentLink->next_;
    }

    if (!currentLink) {
        currentBookRecord = NULL;
    }
    
    return currentBookRecord;
}


/*****************************************
    Authentication
*****************************************/

bool Library::validateUser(string accountNum)
{
    Customer* currentUser;
    // search for user
    currentUser = searchCustomer(accountNum);
    // if search user returns user, set current user to user
    if (currentUser) {
        currentCustomer = currentUser; 
        return true;
    }
    return false;
}

bool Library::validateAdmin(string pass)
{
    if (pass == adminPass) {
        currentCustomer = NULL;
        return true;
    } else {
        return false;
    }
} 

void Library::createAccount()
{
    Customer* newCustomer;
    string fName, lName, accountNum;

    cout << "Create your account" << endl;
    cout << "Enter your first name: ";
    cin >> fName;

    cout << "Enter your last name: ";
    cin >> lName;

    cout << "Pick your account number (yep, anything you can remember): ";
    cin >> accountNum;

    cout << "Creating account for " << fName << " " << lName << " "
         << accountNum << endl;

    newCustomer = new Customer(fName, lName, accountNum);

    cout << "Created account. You can login now." << endl; 

    customerList.AddLinkToBack(newCustomer);
}

/*****************************************
    Displays
*****************************************/

void Library::printBooks()
{
    Node* currentLink;
    BookRecord* currentBook;

    currentLink = bookRecords.GetFirstNode();

    cout << "List of books in record" << endl;
    cout << "_______________________" << endl;
    static const char* format = "%-10s %-20s %-35s %-5s %-5s\n";
    printf(format, "Catalog#", "Author", "Title", "In", "Out");


    while (currentLink != NULL) {
        currentBook = static_cast<BookRecord*>(currentLink->data_);
        currentBook->printRecord();
        currentLink = currentLink->next_;
    }
}

/*******************************************
    Interfaces
********************************************/

static char _userMenu()
{
    Customer* currentCustomer = Library::currentCustomer;
    char rv;
    cout << "User Menu" << endl;
    cout << "Logged In as: ";
    cout << currentCustomer->getFirstName() << " "
         << currentCustomer->getLastName()  << "|";
    cout << " Books Checked out: " << currentCustomer->getNumBooks();
    cout << " | Late books? " << (currentCustomer->hasLateBooks()? "Yes":"no ")
         << endl;

    cout << "a) checkout book" << endl;
    cout << "b) view books" << endl;
    cout << "c) return book" << endl;
    cout << "d) pay fees" << endl;
    cout << "e) logout" << endl;
    cout << "-> ";
    cin >> rv;
    return rv;
}

void Library::userInterface()
{
    bool userLogout = false;

    while (!userLogout) {
        switch(_userMenu()) {
            case 'a':
                checkoutBook(); 
                break;
            case 'b':
                printBooks(); 
                break;
            case 'c':
                returnBook();
                break;
            case 'd':
                payFee();
                break;
            case 'e':
                userLogout = true;
                currentCustomer = NULL;
                cout << "Logging out" << endl;
                break;
            default:
                cout << "Unrecognized menu command" << endl;
                break; 
        }
    }
    return;
}

static char _adminMenu()
{
    char rv;
    cout << "a) add a book" << endl;
    cout << "b) logout" << endl;
    cout << "-> ";
    cin >> rv;

    return rv;
}


void Library::adminInterface()
{
    bool adminLogout = false;

    while(!adminLogout) {
        switch(_adminMenu()) {
            case 'a':
                createNewBook();
                break;
            case 'b':
                adminLogout = true; 
                cout << "logging out of admin..." << endl;
                break;
            default:
                cout << "Unrecognized menu command" << endl;
                break;
        }
    }
    return;
}



/***************
    Loaders
****************/
char *parseLine(int& place, char buff[], string line_)
{
    int i;
    // place is used to keep place in line_
    // skip white space
    while (line_[place] == ' ')
        place++;
    // loop over characters until end of line_ or you see a comma
    for (i = 0; place < line_.length() && line_[place] != ','; ){
        buff[i++] = line_[place++];
    }
    // null terminate string
    buff[i] = '\0';
    // increment place to move it over the comma 
    place++;
    return buff;

}
void Library::load_books()
{
    ifstream inFile;
    string line_, author, title, catalogNum;
    BookRecord* newBook;
    int in, out;
    int pos_;
    char buff[ENTRY_SZ];

    inFile.open(bookDatabaseFile.c_str());
    if (!inFile) {
        cout << "Coudn't open file" << endl;
        std::exit(1);
    }

    getline(inFile, line_);
    while (inFile) {
        pos_ = 0;
        title = string(parseLine(pos_, buff, line_));
        author = string(parseLine(pos_, buff, line_));
        catalogNum = string(parseLine(pos_, buff, line_));
        in = atoi(parseLine(pos_, buff, line_));
        out = atoi(parseLine(pos_, buff, line_));

        newBook = new BookRecord(catalogNum, author, title, in, out); 
        bookRecords.AddLinkToBack(newBook);
        getline(inFile, line_);
        
    }
}

char *parseUserLine(int& place, char buff[], string line_)
{
    int i;
    // place is used to keep place in line_
    // skip white space
    while (line_[place] == ' ')
        place++;
    // loop over characters until end of line_ or you see a comma
    for (i = 0; place < line_.length() && line_[place] != ',' 
                                       && line_[place] != '>'; ){
        buff[i++] = line_[place++];
    }
    // null terminate string
    buff[i] = '\0';
    // increment place to move it over the comma 
    place++;
    return buff;

}

void Library::load_customers()
{
    ifstream inFile;
    string line_, fName, lName, accountNum, catalogNum, date_checkedout;
    float balance;
    Book* book;
    BookRecord* bookrecord;
    Customer* customer;
    struct tm checkoutTime;
    char buff[ENTRY_SZ];
    int pos_ = 0;

    inFile.open(customerDatabaseFile.c_str());
    if (!inFile) {
        cout << "Couldn't open user file" << endl;
        std::exit(1);    
    }
    
    // initialize tm struct
    checkoutTime.tm_mday = 0;
    checkoutTime.tm_mon = 0;
    checkoutTime.tm_year = 0;
    checkoutTime.tm_sec = 0;
    checkoutTime.tm_min = 0;
    checkoutTime.tm_hour = 0;

    getline(inFile, line_);
    while (inFile) {
        pos_ = 0;
        // this is the user info line
        if (line_[0] == '<') {
            // increment past the <
            pos_++;
            fName = string(parseUserLine(pos_, buff, line_));
            lName = string(parseUserLine(pos_, buff, line_));
            accountNum = string(parseUserLine(pos_, buff, line_));
            balance = atof(parseUserLine(pos_, buff, line_));

            // create the customer object and update their balance
            customer = new Customer(fName, lName, accountNum);
            customer->updateBalance(balance);
            customerList.AddLinkToBack(customer); 
            
        } else if (line_ == "") {
            ;//skip this line
        } else {
            // not a user line, parse the catalogNum and duedate
            catalogNum = string(parseLine(pos_, buff, line_));
            date_checkedout = string(parseLine(pos_, buff, line_));
            
            // find the book in the bookRecords
            bookrecord = searchBook(catalogNum); 
            if (!bookrecord) {
                cout << "Book with catalog #" << catalogNum
                     << " does not exist!" << endl;
                getline(inFile, line_);
                continue;
            }

            // create book and add it to customers list
            book = bookrecord->createBookCopy();
            // Get the checkout date
            strptime(date_checkedout.c_str(), "%D", &checkoutTime);
            book->setCheckoutDate(mktime(&checkoutTime));
            // set the due date to certain number of weeks
            book->setDueDate(WEEK_LIMIT);
    
            customer->addBookToAccount(book);
        }
       
        getline(inFile, line_); 
    }
            
}
        
/********************
*********************/

// local helper function to clear file indicated by path
static void clearFile(string path)
{
    ofstream outFile;
    outFile.open(path.c_str());
    if (!outFile) {
        cout << "Couldn't open outfile: " << path << endl;
        return;
    }

    outFile.close(); 
}
    

void Library::dump_customers()
{
    ofstream outFile;
    Customer* curCustomer;
    Node* currentLink;

    // clear customer dbFile
    clearFile(customerDatabaseFile);
    outFile.open(customerDatabaseFile.c_str());
    if (!outFile) {
        cout << "Couldn't open file customerDBFile for writing" << endl;
        std::exit(1);
    }
    
    currentLink = customerList.GetFirstNode(); 
    // iterate through customers
    while (currentLink != NULL) {
        curCustomer = static_cast<Customer*>(currentLink->data_);
        // write customer line
        curCustomer->appendToFile(outFile);
        currentLink = currentLink->next_;
    }

}

void Library::dump_books()
{
    ofstream outFile;
    BookRecord* curBookRecord;
    Node* currentLink;

    //clear book dbfile
    clearFile(bookDatabaseFile);
    outFile.open(bookDatabaseFile.c_str());
    if (!outFile) {
        cout << "Couldn't open file bookDBFile for writing" << endl;
        std::exit(1);
    }

    currentLink = bookRecords.GetFirstNode();
    // iterate through books
    while (currentLink != NULL) {
        curBookRecord = static_cast<BookRecord*>(currentLink->data_);
        // write book lines
        curBookRecord->appendToFile(outFile);
        currentLink = currentLink->next_;
    }

    return;
}

/*********************
    The loading method
***********************/
void Library::init()
{
    cout << "Loading databases" << endl;
    load_books();
    load_customers();
}
