/*
    Implementation File for a book
*/ 

#include <string>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

#include "Book.h"

#define DAYS_IN_WEEK 7
#define FEE_PER_DAY  1.50

Book::Book(string author, string title, string catalogNum)
{
    author_ = author;    
    title_ = title;
    catalogNum_ = catalogNum;
}


string Book::getTitle()
{
    return title_;
}

string Book::getAuthor()
{
    return author_;
}

string Book::getCatalogNum()
{
    return catalogNum_;
}

bool Book::isOverDue(time_t currentTime) const
{
    return difftime(due_date_, currentTime) < 0;
}

float Book::getLateFee()
{
    int days_over_due;
    float seconds_over_due = difftime(currentLibTime, due_date_) ;
    // calculate the days since overdue
    // seconds/seconds_in_hours/hours_in_day == days
    days_over_due = (seconds_over_due / 3600) / 24;
    
    cout << catalogNum_ << " is " << days_over_due << " Days over due" << endl;
    
    // multiply the days since overdue by a fee
        
    return days_over_due * FEE_PER_DAY;
}

void Book::setCheckoutDate(time_t checkoutDate)
{
    checkout_date_ = checkoutDate;

}

void Book::setDueDate(int nWeeks)
{
    struct tm *ptr;
    const time_t *time_ptr;
    time_t dueDate;

    time_ptr = &checkout_date_;
    // steps, convert checkout date calendar time to broken time
    ptr = localtime(time_ptr);
    // increment broken times weekdays by nWeeks * 7 days
    ptr->tm_mday += nWeeks * DAYS_IN_WEEK; 
    // reconvert this time_t to a calendar time
    dueDate = mktime(ptr);
    // set it to instances due_date
    due_date_ = dueDate;
}

char* Book::getDueDate() const
{
    // convert time_t object to tm object and get a custom string by strftime
    static const char* date_fmt = "%D"; // this is the std american time fmt
    static char _date[255];

    strftime(_date, sizeof(_date), date_fmt, localtime(&due_date_));
    
    return _date;
}

char* Book::getCheckoutDate() const
{
    static const char* date_fmt = "%D";
    static char _date[255];

    strftime(_date, sizeof(_date), date_fmt, localtime(&checkout_date_));
    return _date;
}    


void Book::printBook()
{
    printf("%20s %20s %10s %10s\n", title_.c_str(), author_.c_str(), 
                                    catalogNum_.c_str(), getDueDate());
}

void Book::appendToFile(ofstream& outFile)
{
    if (!outFile) {
        cout << "Couldn't write book to file" << endl;
        std::exit(1);
    }

    outFile << catalogNum_ << ", " << getCheckoutDate() << endl;
}
    
